#!/bin/bash

echo "Updating submodules..."
git submodule update --recursive --remote

dir=$(pwd)
parent="$(dirname "$dir")"
home=$HOME

if [ "$parent" != "$home" ]; then
  printf "This repository must be cloned into and run from $home/Dotfiles."
  exit 1
fi

if hash stow 2>/dev/null; then
  printf "Preparing to symlink all dotfiles...\n\n"
else
  sudo apt install stow
fi 

printf "Installer will link dotfiles for the following:\n"
printf "  (Existing files will not be overwritten)\n\n"
find . -maxdepth 1 -mindepth 1 -type d  -not -path '*/\.*' -printf '* %f\n' 

printf "\n"
read -p "Press [enter] to continue or Ctrl+c to abort"

packages=$(find . -maxdepth 1 -mindepth 1 -type d  -not -path '*/\.*' -printf '%f ')

for pkg in $packages; do
  stow  --verbose=2 $pkg
done

echo "Done!"
