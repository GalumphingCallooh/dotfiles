GalumphingCallooh's dotfiles
===========================

* irssi
* cmus
* conky
* i3-gaps
* rofi
* vim
* nvm
* zsh + oh-my-zsh
* xfce4
* compton
* profanity
* zathura
* + scripts, themes, fonts, wallpapers, bullshit

```bash
# install gnu stow
sudo apt install stow

# clone this repository into your home directory
git clone --recurse-submodules git@gitlab.com:GalumphingCallooh/dotfiles.git ~/Dotfiles

# move into the dotfiles directory
cd ~/Dotfiles

# use stow to manage dotfiles as a symlink farm
stow autostart
stow cmus
stow conky
stow fonts
stow i3-gaps
stow rofi
stow themes
stow vim
stow scripts
stow wallpapers
stow xfce4
stow zsh
stow compton
stow profanity
stow nvm
stow zathura
# etc...

# or you can install all of it
./install-dotfiles.sh

# on a librem keyboard with fucked up pipe character?
cd .librem
sudo stow -t / loadkeys
stow -t $HOME xmodmap
```

![screen](screen1.png)

i3-gaps, xfce4-panel, compton

![screen](screen2.png)

vim, zsh, git, vtop

> Comes with Vundle as a submodule, so run `:PluginInstall` from Vim to get the
> plugins defined in the `.vimrc`.

![screen](screen3.png)

cmus, irssi, profanity
