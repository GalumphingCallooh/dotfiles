let g:airline_theme = 'hybrid'
let g:airline_left_sep = '⮀'
let g:airline_right_sep = '⮂'
let g:airline_symbols = {}
let g:airline_symbols.branch = '⎇'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = '⮀'
let g:airline#extensions#tabline#left_alt_sep = '⮀'
let g:airline#extensions#tabline#right_sep = '⮂'
let g:airline#extensions#tabline#right_alt_sep = '⮂'

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'nathanaelkane/vim-indent-guides'
Plugin 'tpope/vim-fugitive'
Plugin 'scrooloose/nerdtree'
Plugin 'pangloss/vim-javascript'
Plugin 'Lokaltog/vim-easymotion'
Plugin 'kien/ctrlp.vim'
Plugin 'scrooloose/syntastic'
Plugin 'tomlion/vim-solidity'
Plugin 'https://github.com/ervandew/supertab.git'
Plugin 'https://github.com/vim-scripts/AutoClose.git'
Bundle 'https://github.com/jistr/vim-nerdtree-tabs.git'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'rust-lang/rust.vim'
Plugin 'leafgarland/typescript-vim'
Plugin 'https://github.com/digitaltoad/vim-pug.git'
call vundle#end()

set background=dark
set nocompatible
colorscheme jellybeans
syntax enable
set expandtab
set shiftwidth=2
set softtabstop=2
filetype indent on
filetype plugin on
set list
set listchars=tab:▸\ ,eol:¬
set nu
set showcmd
set cursorline
set cursorcolumn
set wildmenu
set lazyredraw
set showmatch
set incsearch
set hlsearch
nnoremap <leader><space> :nohlsearch<CR>
nnoremap <space> za

autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif
map <C-n> <plug>NERDTreeTabsToggle<CR>

function! <SID>StripTrailingWhitespaces()
  let _s=@/
  let l = line(".")
  let c = col(".")
  %s/\s\+$//e
  let @/=_s
  call cursor(l, c)
endfunction

autocmd BufWritePre *.rb,*.py,*.js :call <SID>StripTrailingWhitespaces()
highlight OverLength ctermbg=red ctermfg=white guibg=#592929
match OverLength /\%81v.\+/

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_javascript_checkers = ['jshint']
let g:syntastic_python_checkers = ['pylint']
"let g:syntastic_python_pylint_args = '--load-plugins pylint_django'
