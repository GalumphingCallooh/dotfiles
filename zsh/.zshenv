# Path to Rust Cargo binaries
  export PATH="$HOME/.cargo/bin:$PATH"
# Path to user binaries
  export PATH="$HOME/.local/bin:$PATH"
