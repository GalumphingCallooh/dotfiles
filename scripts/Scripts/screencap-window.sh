#!/bin/bash

notify-send -i window -t 3000 "Byzanz" "Select window to capture as GIF..."

XWININFO=$(xwininfo)

read X <<< $(awk -F: '/Absolute upper-left X/{print $2}' <<< "$XWININFO")
read Y <<< $(awk -F: '/Absolute upper-left Y/{print $2}' <<< "$XWININFO")
read W <<< $(awk -F: '/Width/{print $2}' <<< "$XWININFO")
read H <<< $(awk -F: '/Height/{print $2}' <<< "$XWININFO")

FILE_PATH=~/Pictures/Screenshots/Captures/screencap-`/bin/date +%Y%m%d-%H:%M:%S`.gif

notify-send -i time -t 2000 "Byzanz" "Capturing window as GIF in 3 seconds..."
sleep 3
notify-send -i video -t 1 "Byzanz" "Capturing window as GIF"
byzanz-record --delay 0 --x=$X --y=$Y --width=$W --height=$H $FILE_PATH
notify-send -i folder -t 3000 "Byzanz" "GIF saved to $FILE_PATH"
