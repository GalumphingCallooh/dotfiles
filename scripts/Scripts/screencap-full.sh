#!/bin/sh

FILE_PATH=~/Pictures/Screenshots/Captures/screencap-`/bin/date +%Y%m%d-%H:%M:%S`.gif

notify-send -i time -t 2000 "Byzanz" "Capturing screen as GIF in 3 seconds..."
sleep 3
notify-send -i video -t 1 "Byzanz" "Capturing screen as GIF"
byzanz-record --delay 0 $FILE_PATH
notify-send -i folder -t 3000 "Byzanz" "GIF saved to $FILE_PATH"
